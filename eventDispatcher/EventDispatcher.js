(function() {
    'use strict';

    function inArray(arr, match, key) {
        var inArray = false;

        arr.forEach(function(item) {
            if (item[key] === match) inArray = true;
        });

        return inArray;
    }

    window.EventDispatcher = function() {
        this.listeners = {};
        this.triggers = [];
        this.triggered = [];

        this.on = function(options) {
            var event = options.event,
                callback = options.callback,
                data = options.data || {};

            if (!this.listeners[event]) this.listeners[event] = [];

            this.listeners[event].push({
                event: event,
                callback: callback,
                data: data
            });
        };

        this.trigger = function(options) {
            var event = options.event,
                data = options.data || {};

            this.triggers.push({
                name: event,
                data: data
            });
        };

        this.ready = function() {
            for (var i = 0; i < this.triggers.length; i++) {
                var triggerItem = this.triggers[i],
                    triggerName = triggerItem.name;

                if (this.listeners[triggerName] === undefined) return;
                if (inArray(this.triggered, triggerName, 'name')) return;

                this.listeners[triggerName].forEach(function(listener) {
                    listener.callback();
                });

                this.triggered.push(triggerItem);
            }
        }
    };
})();
