(function() {
    'use strict';

    var ed = new window.EventDispatcher(),
        console = window.console;

    $(document).ready(function() {
        ed.ready();
    });


    /* Let's begin trigger and subscribe to events */


    /* Initialize UI and trigger event 'ui:ready' */
    (function initUI() {
        console.log('UI ready');
        ed.trigger({
            event: 'ui:ready'
        });
    })();
    
    
    ed.on({
        event: 'ui:ready',
        callback: function() {
            console.log('UI ready, initialize base layout');
            ed.trigger({
                event: 'base-layout:ready'
            });
        }
    });


    /* Subscribe to 'ui:ready' event */
    ed.on({
        event: 'ui:ready',
        callback: function(data) {
            console.log('UI ready, add some fancy datepickers', data);
        }
    });


    /* Subscribe to 'jquery-button-listeners:added' event */
    ed.on({
        event: 'jquery-listeners:added',
        callback: function(data) {
            console.log('jQuery listeners added, trigger some jquery events', data);

            /* This trigger will not executed (because recursion) */
            ed.trigger({
                event: 'ui:ready'
            });
        }
    });


    /* After 'base-layout:ready' register jquery event listeners */
    ed.on({
        event: 'base-layout:ready',
        callback: function(data) {
            (function addJqueryButtonListeners() {
                console.log('Base layout ready, add some jquery event listeners', data);

                /* Trigger 'jquery-button-listeners:added' event inside other event subscriber */
                ed.trigger({
                    event: 'jquery-listeners:added'
                });
            })();
        }
    });

})();
